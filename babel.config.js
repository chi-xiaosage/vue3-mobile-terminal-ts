module.exports = {
  presets: ['@vue/cli-plugin-babel/preset'],
  // vant ui按需导入配置
  plugins: [
    [
      'import',
      {
        libraryName: 'vant',
        libraryDirectory: 'es',
        style: true
      }
    ]
  ]
}
