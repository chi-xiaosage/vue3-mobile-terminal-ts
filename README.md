# 页面展示

**首页**

<img src="assets/image-20220730161319089.png" alt="image-20220730161319089" style="zoom:67%;" />

视频页

<img src="assets/image-20220730161459693.png" alt="image-20220730161459693" style="zoom:80%;" />



# vue3-ts-bilibili

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
