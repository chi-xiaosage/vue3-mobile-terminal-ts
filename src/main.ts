import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

// 引入样式
import '@/assets/styles/base.less'
import '@/assets/styles/iconfont.less'
// 引入mockjs
import '@/mock/index'

// 导入Vant组件
import { Tab, Tabs, Swipe, SwipeItem } from 'vant'

const app = createApp(App)

// 注册Vant组件
app.use(Tab)
app.use(Tabs)
app.use(Swipe)
app.use(SwipeItem)

app.use(router)
app.mount('#app')
